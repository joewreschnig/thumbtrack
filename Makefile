#!/usr/bin/make -rf

SKETCH := $(firstword $(wildcard *.ino))
OUTDIR ?= output
OUTPUT := $(addprefix $(OUTDIR)/$(SKETCH),.elf .map .uf2)
SRC := $(wildcard *.c *.h *.cpp *.hpp)

all: $(OUTPUT)

$(addprefix $(OUTDIR)/%.ino,.elf .map .uf2): %.ino $(SRC)
	arduino-cli compile --warnings all --output-dir $(@D) $<
