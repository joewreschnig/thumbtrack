#pragma once

#include <stdint.h>

struct input {
  int pressed_[6];

  struct state {
    uint8_t pressed_, triggered_;

    // Mask against all keys pressed at last input_tick.
    uint8_t pressed(uint8_t mask) { return this->pressed_ & mask; }

    // Mask against all keys first pressed at the last input tick, or at
    // least delay ticks ago, at rate tick intervals. This can be used to
    // generate repeat "presses" without an event stream.
    uint8_t triggered(uint8_t mask) { return this->triggered_ & mask; }
  };


  // Read and return current input state.
  //
  // A decent first guess at a repeat interval is ~250ms delay and ~50ms
  // rate, e.g. ~16/4 if at 60fps.
  state tick(int delay, int rate);
};
