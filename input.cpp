#include <Thumby.h>

#include "input.hpp"

input::state input::tick(int delay, int rate) {
  uint8_t pressed =
    (!digitalRead(THUMBY_BTN_LDPAD_PIN) * BUTTON_L)
    | (!digitalRead(THUMBY_BTN_RDPAD_PIN) * BUTTON_R)
    | (!digitalRead(THUMBY_BTN_UDPAD_PIN) * BUTTON_U)
    | (!digitalRead(THUMBY_BTN_DDPAD_PIN) * BUTTON_D)
    | (!digitalRead(THUMBY_BTN_A_PIN) * BUTTON_A)
    | (!digitalRead(THUMBY_BTN_B_PIN) * BUTTON_B);
  uint8_t triggered = 0;
  for (size_t i = 0; i < std::size(this->pressed_); i++) {
    int on = !!(pressed & (1 << i));
    this->pressed_[i] = this->pressed_[i] * on + on;
    triggered |= ((this->pressed_[i] == 1)
                  | (((this->pressed_[i] - delay) % rate) == 1))
      << i;
  }
  return state{pressed, triggered};
}
