#include <string.h>
#include <stdio.h>

#include <hardware/pwm.h>
#include <Thumby.h>

#include "input.hpp"
#include "sprite8.hpp"

static Thumby thumby;
static input inp;

struct track {
  uint8_t data[60];
  uint8_t interval;
  uint8_t end;
  uint8_t pos;

  void speed_up() {
    if (this->interval > 2) {
      this->interval--;
    }
  }

  void slow_down() {
    if (this->interval < 50) {
      this->interval++;
    }
  }

  void shrink() {
    if (this->end > 1) {
      this->end--;
    }
  }

  void extend() {
    if (this->end < std::size(this->data)) {
      this->end++;
    }
  }
};

static struct {
  track tracks[3];
  int freq;
  unsigned selected;
  unsigned frame;
} player;


const uint32_t TICK_INTERVAL = 20;

void setup() {
  thumby.begin();
  player.freq = 96;
  for (auto &track : player.tracks) {
    track.interval = 10;
    track.end = std::size(track.data);
  }
}

static float freq_A0_19TET(uint8_t n) {
  return n ? 13.75f * powf(2, (n-1)/19.f) : 0;
}

static void play_note(uint8_t n) {
  static uint32_t hz = clock_get_hz(clk_sys);
  static uint8_t slice_num = pwm_gpio_to_slice_num(THUMBY_AUDIO_PIN);
  static uint8_t current;

  // While wrap/level are double-buffered and won't affect the phase if
  // set redundantly, the documentation doesn't say the same for
  // set_clkdiv or set_enabled. Avoid touching the PWM settings if we
  // don't have to.
  //
  // Easiest to detect here - if we wanted to avoid this in the track
  // selection loop we'd need to check not just the previous node, but
  // the previous note on the previous track.
  if (n == current) { return; }

  current = n;

  float freq = freq_A0_19TET(n);
  float div = ceil((hz / freq) / (UINT16_MAX + 1));
  uint32_t top = hz / div / freq - 1;

  pwm_set_clkdiv(slice_num, div);
  pwm_set_wrap(slice_num, top);
  pwm_set_chan_level(slice_num, PWM_CHAN_A, top/2);

  pwm_set_enabled(slice_num, n > 0);
}

void loop() {
  absolute_time_t next_tick = make_timeout_time_ms(TICK_INTERVAL);

  player.frame++;
  for (auto &track : player.tracks) {
    if (player.frame % track.interval == 0) {
      track.pos = (track.pos + 1) % track.end;
    }
  }

  auto state = inp.tick(250/TICK_INTERVAL, 50/TICK_INTERVAL);
  track &selected = player.tracks[player.selected];

  if (state.pressed(BUTTON_B)) { // shift mode
    if (state.pressed(BUTTON_A)) {
      selected.data[selected.pos] = 0;
    }
    if ((state.triggered(BUTTON_L))) {
      selected.slow_down();
    }
    if ((state.triggered(BUTTON_R))) {
      selected.speed_up();
    }
    if ((state.triggered(BUTTON_U))) {
      selected.extend();
    }
    if ((state.triggered(BUTTON_D))) {
      selected.shrink();
    }

  } else {
    if (state.pressed(BUTTON_A)) {
      // Play button presses tend to anticipate and push the beat. If
      // it's pressed within the last quarter of the interval affect the
      // next position instead.
      int pos = selected.pos;
      if ((player.frame % selected.interval) > (selected.interval * 3 / 4)) {
        pos = (pos + 1) % std::size(selected.data);
      }
      selected.data[pos] = player.freq;
    }
    if (state.triggered(BUTTON_U)) {
      player.selected = (player.selected - 1) % std::size(player.tracks);
    }
    if (state.triggered(BUTTON_D)) {
      player.selected = (player.selected + 1) % std::size(player.tracks);
    }
    if ((state.triggered(BUTTON_L)) && player.freq > 2) {
      player.freq--;
    }
    if ((state.triggered(BUTTON_R)) && player.freq < 190) {
      player.freq++;
    }
  }

  // If we use a fixed order for track preferences, even starting at any
  // position in it modulo frame count will bias for a track after an
  // un-playing track. Instead we need to make sure we pick uniformly
  // from the active tracks.
  //
  // Instead we need to pick based on combination of frame number and
  // number of active tracks. Count active tracks and then pick one.
  // (There may be a smarter way to do this.)
  size_t count = 0;
  for (auto &track : player.tracks) {
    count += track.data[track.pos] > 0;
  }
  if (count > 0) {
    size_t idx = player.frame % count;
    // When it underflows, we're done.
    for (auto &track : player.tracks) {
      if (track.data[track.pos] && idx-- == 0) {
        play_note(track.data[track.pos]);
      }
    }
  } else {
    play_note(0);
  }

  thumby.clear();

  sprite8_draw(&thumby, indicator,
               1 + ((player.frame % 100) < 40), 7 * player.selected);

  int16_t x0 = 9; // just past the indicator

  for (size_t i = 0; i < std::size(player.tracks); i++) {
    track &track = player.tracks[i];
    int16_t y0 = 7 * i; // 6px high + 1px spacing

    // draw a 2px high track
    for (int y = y0 + 2; y < y0 + 4; y++) {
      thumby.goTo(x0, y);
      for (int x = 0; x < track.end; x++) {
        thumby.writePixel(!!track.data[x]);
      }
    }

    // draw markers for current position outside it
    thumby.drawPixel(x0 + track.pos, y0 + 1, 1);
    thumby.drawPixel(x0 + track.pos, y0 + 4, 1);

    // draw endcap
    for (int dy = 0; dy < 6; dy++) {
      thumby.drawPixel(x0 + track.end, y0 + dy, 1);
    }
  }

  // show current frequency / selected track info
  char buf[10];
  float hz = freq_A0_19TET(player.freq);
  if (hz >= 10000) {
    snprintf(buf, std::size(buf), "%.1fkHz", hz/1000.f);
  } else if (hz >= 1000) {
    snprintf(buf, std::size(buf), "%.2fkHz", hz/1000.f);
  } else if (hz > 100) {
    snprintf(buf, std::size(buf), "%.0fHz", hz);
  } else {
    snprintf(buf, std::size(buf), "%.1fHz", hz);
  }
  thumby.setCursor(0, 27);
  thumby.print(buf);

  snprintf(buf, std::size(buf), "%db", selected.end);
  thumby.setCursor(THUMBY_SCREEN_WIDTH - thumby.getPrintWidth(buf), 22);
  thumby.print(buf);

  snprintf(buf, std::size(buf), "/%.1fs",
           (selected.interval * TICK_INTERVAL * selected.end) / 1000.f);
  thumby.setCursor(THUMBY_SCREEN_WIDTH - thumby.getPrintWidth(buf), 31);
  thumby.print(buf);

  thumby.writeBuffer(thumby.getBuffer(), thumby.getBufferSize());

  sleep_until(next_tick);
}
