#pragma once

#include <stdint.h>

#include <GraphicsBuffer.h>

#define sprite8(r0, r1, r2, r3, r4, r5, r6, r7) \
  ((uint64_t)(0b##r0) << 56) | \
  ((uint64_t)(0b##r1) << 48) | \
  ((uint64_t)(0b##r2) << 40) | \
  ((uint64_t)(0b##r3) << 32) | \
  ((uint64_t)(0b##r4) << 24) | \
  ((uint64_t)(0b##r5) << 16) | \
  ((uint64_t)(0b##r6) <<  8) | \
  ((uint64_t)(0b##r7) <<  0)

const uint64_t indicator =
  sprite8(10000000,
          11000000,
          01100000,
          01100000,
          11000000,
          10000000,
          00000000,
          00000000);

static void sprite8_draw(GraphicsBuffer *buf, uint64_t spr, uint8_t x, uint8_t y) {
  buf->setX(x, x + 7);
  buf->setY(y, y + 7);
  for (uint64_t i = (uint64_t)1 << 63; i != 0; i >>= 1) {
    buf->writePixel((spr & i) != 0);
  }
}
