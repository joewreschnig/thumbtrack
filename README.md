A weird audio tracker/sequencer for the [Thumby][], which has a 1 bit
piezo speaker.

There are three tracks available, each of which can play back at
different speeds and with different loop lengths.


## Controls

- **Up / Down:** Select track for editing
- **Left / Right:** Select frequency
- **A:** Play at the current position on the selected track

- **B+Left / B+Right:** Slow down / speed up the selected track
- **B+Down / B+Up:** Adjust the loop length of the selected track
- **B+A:** Silence the current position on the selected track


## Building

After setting up your [Arduino CLI][] environment and installing [Thumby-Lib][]
and [GraphicsBuffer-Lib][],

```
$ make
```

But I don’t really know what I’m doing as this is my first Arduino
project, so if you know a better way, do that.


## License

Copyright 2022 Joe Wreschnig

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.


[Arduino CLI]: https://github.com/arduino/arduino-cli/
[GraphicsBuffer-Lib]: https://github.com/TinyCircuits/TinyCircuits-GraphicsBuffer-Lib
[Thumby-Lib]: https://github.com/TinyCircuits/TinyCircuits-Thumby-Lib/
[Thumby]: https://thumby.us/
